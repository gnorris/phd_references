# PhD References

References used during the PhD of Graham Norris.

## Usage notes

- Set your local (polybox) file directory in JabRef using *Library* > *Library Properties* > *User-specific file directory*
- Save names in lastname, firstname order
- Convert unicode characters in names to LaTex equivalents
- Follow normal bibtexkey convention
- Do not use unicode characters in bibtexkey
- Abbreviate journal names (to ISO 4 standard)
- File links should be relative to the `indexed` folder
- File names should match bibtexkey (no unicode)
- PDF files should have advertising or cover pages removed and have the supplementary material appended at the end of the file
- A DOI is perferred over a URL
- Set titles in lowercase letters exlcuding proper nouns, names, etc. Set any capitalized words in curly braces `{}`
- ArXiv entries can be imported using the ArXiv importer tool (*New entry* > *ID type: ArXiv*), but please reorder the names and **delete the abstract and keywords** from the bibtex source
- For ArXiv entries, set the Journal field to `arXiv:arxiv_id_number`
- Feel free to add or reorganize groups as needed
